from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin
from django import forms
from registration.forms import (
    CustomCellAdminForm, CustomAssociateUserAdminForm,
    CustomEventInvitationAdminForm
)
from registration.models import (
    User, AssociateUser, ExecutiveUser, DirectorUser, SeniorUser, ContactList,
    Event, EventInvitation, Cell
)
from django.contrib.auth.models import Group

class CellAdmin(admin.ModelAdmin):
    form = CustomCellAdminForm

class UserAdmin(BaseUserAdmin):
    pass

class BaseUserAdmin(admin.ModelAdmin):

    user_type = ['a', 'e', 'd', 's']
    group = 1
    user = AssociateUser

    def get_queryset(self, request):
        if request.user.type in self.user_type:
            return self.user.objects.filter(id=request.user.id)
        return self.user.objects.all()

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(
            request, obj, **kwargs
        )
        form.current_user = request.user
        return form

    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(
            request, obj
        )

        if request.user.type == 'e':
            fieldsets[0][1]['fields'].pop()
        return fieldsets

    def save_model(self, request, obj, form, change):
        if not change:
            obj.save()
            new_group = Group.objects.get(id=self.group)
        else:
            old_group = obj.groups.first()
            new_group = Group.objects.get(
                id=request.POST.get('group', self.group)
            )
            if old_group != new_group:
                obj.groups.remove(old_group)

        obj.groups.add(new_group)
        obj.type = new_group.name[0].lower()        
        obj.save()

class AssociateUserAdmin(BaseUserAdmin):
    form = CustomAssociateUserAdminForm

    user_type = ['a']
    group = 1
    user = AssociateUser

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class ExecutiveUserAdmin(BaseUserAdmin):
    form = CustomAssociateUserAdminForm

    user_type = ['e']
    user = ExecutiveUser
    group = 2

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class DirectorUserAdmin(BaseUserAdmin):
    form = CustomAssociateUserAdminForm

    user_type = ['d']
    user = DirectorUser
    group = 3

class SeniorUserAdmin(BaseUserAdmin):
    form = CustomAssociateUserAdminForm

    user_type = ['s']
    user = SeniorUser
    group = 4

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class ContactListAdmin(admin.ModelAdmin):
    model = ContactList

    fields = (
        'contact',
    )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(list_owner=request.user)

    def save_model(self, request, obj, form, change):
        obj.list_owner = request.user
        super().save_model(
            request, obj, form, change
        )

class EventAdmin(admin.ModelAdmin):
    model = Event

    fields = (
        'name', 'date'
    )

class EventInvitationAdmin(admin.ModelAdmin):
    model = EventInvitation
    form = CustomEventInvitationAdminForm

    list_display = ('event', 'receiver', 'is_confirmed')
    fields = ('event', 'receiver',)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(
            request, obj, **kwargs
        )
        form.current_user = request.user
        return form

    def save_model(self, request, obj, form, change):
        obj.sender = request.user
        super().save_model(
            request, obj, form, change
        )

admin.site.register(Cell, CellAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(ContactList, ContactListAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(AssociateUser, AssociateUserAdmin)
admin.site.register(ExecutiveUser, ExecutiveUserAdmin)
admin.site.register(DirectorUser, DirectorUserAdmin)
admin.site.register(SeniorUser, SeniorUserAdmin)
admin.site.register(EventInvitation, EventInvitationAdmin)