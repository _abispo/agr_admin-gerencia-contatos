from django import forms
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.admin.widgets import FilteredSelectMultiple

User = get_user_model()

from registration.models import (
    Cell, DirectorUser, AssociateUser, EventInvitation, ContactList
)

class CustomAssociateUserAdminForm(forms.ModelForm):
    class Meta:
        model = AssociateUser
        fields = (
            'username', 'first_name', 'last_name', 'email', 'note',
            'is_staff', 'is_active'
        )

    group = forms.ModelChoiceField(
        queryset=Group.objects.all(),
        required=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.current_user.type == 'd':
            queryset = Group.objects.filter(
                name__in=['Associados', 'Executivos']
            )

        elif self.current_user.type == 's':
            queryset = Group.objects.filter(
                name__in=['Associados', 'Executivos', 'Diretores']
            )

        else:
            queryset = Group.objects.all()
        
        self.fields['group'].queryset = queryset
        self.fields['group'].empty_label = None
        if self.instance.id is not None:
            self.fields['group'].initial = self.instance.groups.first().id

class CustomCellAdminForm(forms.ModelForm):
    class Meta:
        model = Cell
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['director'].queryset = DirectorUser.objects.all()

class CustomEventInvitationAdminForm(forms.ModelForm):
    class Meta:
        model = EventInvitation
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CustomEventInvitationAdminForm, self).__init__(*args, **kwargs)
        self.fields['receiver'].queryset = ContactList.objects.filter(
            list_owner = self.current_user
        )