from django.db import models
from django.contrib.auth.base_user import BaseUserManager

class AssociateUserManager(BaseUserManager):    
    def get_queryset(self):
        return super().get_queryset().filter(
            type='a'
        )

class ExecutiveUserManager(BaseUserManager):    
    def get_queryset(self):
        return super().get_queryset().filter(
            type='e'
        )

class DirectorUserManager(BaseUserManager):
    def get_queryset(self):
        return super().get_queryset().filter(
            type='d'
        )

class SeniorUserManager(BaseUserManager):
    def get_queryset(self):
        return super().get_queryset().filter(
            type='s'
        )

class CellManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(director__type='d')