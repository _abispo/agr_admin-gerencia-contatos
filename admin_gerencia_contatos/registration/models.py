from django.db import models
from django.contrib.auth.models import AbstractUser
from registration.managers import (
    AssociateUserManager, ExecutiveUserManager, DirectorUserManager,
    SeniorUserManager, CellManager
)

USER_TYPE= (
    ('a', 'Associado'),
    ('e', 'Executivo'),
    ('d', 'Diretor'),
    ('s', 'Sênior'),
    ('x', 'Admin')
)

class User(AbstractUser):
    type = models.CharField(max_length=1, choices=USER_TYPE, default='a')
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(max_length=255, blank=False, null=False)
    note = models.TextField(max_length=500, blank=False, null=False)

class AssociateUser(User):

    objects = AssociateUserManager()
    class Meta:
        proxy = True
        verbose_name = 'Associado'
        verbose_name_plural = 'Associados'

class ExecutiveUser(User):

    objects = ExecutiveUserManager()

    class Meta:
        proxy = True
        verbose_name = 'Executivo'
        verbose_name_plural = 'Executivos'

class DirectorUser(User):
    objects = DirectorUserManager()

    class Meta:
        proxy = True
        verbose_name = 'Diretor'
        verbose_name_plural = 'Diretores'

class SeniorUser(User):
    objects = SeniorUserManager()
    class Meta:
        proxy = True
        verbose_name = 'Sênior'
        verbose_name_plural = 'Seniôres'

class ContactList(models.Model):
    list_owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    related_name='contact_list')
    contact = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "{} {}({})".format(
            self.contact.first_name,
            self.contact.last_name,
            self.contact.email
        )

    class Meta:
        verbose_name = 'Contato'
        verbose_name_plural = 'Meus Contatos'

class Event(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    date = models.DateTimeField()

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<Event '{}'>".format(self.name)

class EventInvitation(models.Model):
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='event',
        verbose_name='Evento'
    )

    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_sender')

    receiver = models.ForeignKey(
        ContactList,
        on_delete=models.CASCADE,
        related_name='contacts',
        verbose_name='Para')
    is_confirmed = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Convite'
        verbose_name_plural = 'Convites'

class Cell(models.Model):
    objects = CellManager()
    name = models.CharField(max_length=255, blank=False, null=False)
    address = models.CharField(max_length=255, blank=False, null=False)
    director = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<Cell '{}'>".format(self.name)

    class Meta:
        verbose_name = 'Célula'
        verbose_name_plural = 'Células'